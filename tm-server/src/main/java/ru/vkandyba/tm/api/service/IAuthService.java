package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.dto.UserDTO;

public interface IAuthService {

    UserDTO getUser();

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    void checkRoles(Role... roles);
}
