package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.endpoint.IAdminEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void createUser(@NotNull String login, @NotNull String password, @NotNull String email) {
        serviceLocator.getUserDtoService().create(login, password, email);
    }

    @Override
    public void removeUser(@NotNull SessionDTO sessionDTO, @NotNull String login) {
        serviceLocator.getSessionDtoService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserDtoService().removeByLogin(login);
    }

    @Override
    public void lockUser(@NotNull SessionDTO sessionDTO, @NotNull String login) {
        serviceLocator.getSessionDtoService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserDtoService().lockUserByLogin(login);
    }

    @Override
    public void unLockUser(@NotNull SessionDTO sessionDTO, @NotNull String login) {
        serviceLocator.getSessionDtoService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getUserDtoService().unlockUserByLogin(login);
    }
}
