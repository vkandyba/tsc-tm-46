package ru.vkandyba.tm.repository.dto;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.ProjectDTO;
import ru.vkandyba.tm.enumerated.Status;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDtoRepository extends AbstractDtoRepository<ProjectDTO> {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public List<ProjectDTO> findAll(@NotNull String userId){
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if(list.isEmpty())
            return null;
        return list;
    }

    public ProjectDTO findById(@NotNull String userId, @NotNull String id){
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public ProjectDTO findByName(@NotNull String userId, @NotNull String name){
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public void removeByName(@NotNull String userId, @NotNull String name){
        ProjectDTO projectDTO = findByName(userId, name);
        remove(projectDTO);
    }

    public void startById(@NotNull String userId, @NotNull String id){
        ProjectDTO projectDTO = findById(userId, id);
        projectDTO.setStatus(Status.IN_PROGRESS);
        update(projectDTO);
    }

    public void startByName(@NotNull String userId, @NotNull String name){
        ProjectDTO projectDTO = findByName(userId, name);
        projectDTO.setStatus(Status.IN_PROGRESS);
        update(projectDTO);
    }

    public void finishById(@NotNull String userId, @NotNull String id){
        ProjectDTO projectDTO = findById(userId, id);
        projectDTO.setStatus(Status.COMPLETED);
        update(projectDTO);
    }

    public void finishByName(@NotNull String userId, @NotNull String name){
        ProjectDTO projectDTO = findByName(userId, name);
        projectDTO.setStatus(Status.COMPLETED);
        update(projectDTO);
    }

    public void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description){
        ProjectDTO projectDTO = findById(userId, id);
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);
    }

    public void clear(@NotNull String userId){
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
