package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskService{

    List<TaskDTO> findAll(String userId);

    TaskDTO findByName(String userId, String name);

    TaskDTO findById(String userId, String id);

    void add(String userId, TaskDTO task);

    void removeByName(String userId, String name);

    void removeById(String userId, String id);

    void updateById(String userId, String id, String name, String description);

    void startById(String userId, String id);

    void startByName(String userId, String name);

    void finishById(String userId, String id);

    void finishByName(String userId, String name);

    void clear(String userId);

}
