package ru.vkandyba.tm.repository.dto;

import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.AbstractEntityDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractDtoRepository<E extends AbstractEntityDTO> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDtoRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable E entity){
        if(entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable E entity){
        if(entity == null) return;
        entityManager.merge(entity);
    }

    public void remove(@Nullable E entity){
        if(entity == null) return;
        entityManager.remove(entity);
    }

}
