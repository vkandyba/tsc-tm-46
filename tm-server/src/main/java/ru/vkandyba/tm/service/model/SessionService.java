package ru.vkandyba.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.repository.model.SessionRepository;
import ru.vkandyba.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class SessionService extends AbstractService {

    @NotNull
    private final UserService userService;

    public SessionService(@NotNull IConnectionService connectionService, @NotNull UserService userService) {
        super(connectionService);
        this.userService = userService;
    }

    public void close(@Nullable final Session session) {
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final SessionRepository repository = new SessionRepository(entityManager);
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = userService.findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final SessionRepository repository = new SessionRepository(entityManager);
            repository.add(session);
            entityManager.getTransaction().commit();
            return sign(session);
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        final User user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(connectionService.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @SneakyThrows
    public void validate(Session session) {
        if(session == null) throw new AccessDeniedException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if(session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check) throw new AccessDeniedException();
    }

    public void validate(Session session, Role role) {
        validate(session);
        @Nullable final User user = userService.findByLogin(session.getUser().getId());
        if(user == null) throw new AccessDeniedException();
        if(!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    public @Nullable Session sign(Session session) {
        if(session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(connectionService.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }

}
