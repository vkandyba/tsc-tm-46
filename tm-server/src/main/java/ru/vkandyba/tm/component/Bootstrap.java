package ru.vkandyba.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.endpoint.*;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.endpoint.*;
import ru.vkandyba.tm.service.*;
import ru.vkandyba.tm.service.dto.ProjectDtoService;
import ru.vkandyba.tm.service.dto.SessionDtoService;
import ru.vkandyba.tm.service.dto.TaskDtoService;
import ru.vkandyba.tm.service.dto.UserDtoService;
import ru.vkandyba.tm.service.model.ProjectService;
import ru.vkandyba.tm.service.model.SessionService;
import ru.vkandyba.tm.service.model.TaskService;
import ru.vkandyba.tm.service.model.UserService;
import ru.vkandyba.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ProjectDtoService projectDtoService = new ProjectDtoService(connectionService);
    @NotNull
    private final TaskDtoService taskDtoService = new TaskDtoService(connectionService);
    @NotNull
    private final UserDtoService userDtoService = new UserDtoService(connectionService);
    @NotNull
    private final SessionDtoService sessionDtoService = new SessionDtoService(connectionService, userDtoService);
    @NotNull
    private final TaskService taskService = new TaskService(connectionService);
    @NotNull
    private final ProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private final UserService userService = new UserService(connectionService);
    @NotNull
    private final SessionService sessionService = new SessionService(connectionService, userService);

    @SneakyThrows
    private void initPID(){
        @NotNull final String fileName = "tsc-tm.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes() );
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        logService.debug("Test environment.");
        initPID();
        initEndpoint();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public UserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public SessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public ProjectDtoService getProjectDtoService() {
        return projectDtoService;
    }

    @NotNull
    @Override
    public TaskDtoService getTaskDtoService() {
        return taskDtoService;
    }

    @NotNull
    @Override
    public UserDtoService getUserDtoService(){
        return userDtoService;
    }

    @NotNull
    @Override
    public SessionDtoService getSessionDtoService() {
        return sessionDtoService;
    }
}
